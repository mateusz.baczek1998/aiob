FROM python:3.10

COPY requirements.txt .
RUN pip install -r requirements.txt

RUN mkdir /app
WORKDIR /app

COPY backend ./backend
COPY run_backend.py .

