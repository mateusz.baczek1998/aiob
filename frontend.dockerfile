FROM archlinux:latest

RUN pacman -Sy npm gcc --noconfirm

COPY frontend/react-app /app
WORKDIR /app
RUN npm install
RUN npm run build

RUN ls
WORKDIR /app/build

CMD npx --yes serve

