import requests
import base64


url = "https://baczek.me:9081/realms/master/protocol/openid-connect/token"
secret = "gKsjyZpfxUNSY14YNyjOWvylXoCfsATr"
client_id = "hydroponics-controller"
# client_id = "service-account-hydroponics-controller"
auth_token = base64.b64encode(f"{client_id}:{secret}".encode())
print(auth_token)
print(base64.b64decode(auth_token))

data = {
    # "Authorization": f"Basic {auth_token}",
    "client_id": client_id,
    "client_secret": secret,
    "Content-Type": "application/x-www-form-urlencoded",
    "grant_type": "client_credentials",
}

r = requests.post(url, data=data)

token = r.json()["access_token"]
print("Token", token)

url = "https://baczek.me:9001/metrics/1"

headers = {"Authorization": f"jwt {token}", "Content-Type": "application/json"}

data = {
    "water_level": 1,
    "temperature": 1,
    "ph_level": 1,
    "tds_level": 1,
    "water_level_min": 1,
    "water_level_max": 1,
    "light_level": 1,
}
import json

r = requests.post(url, headers=headers, data=json.dumps(data))
print(r.text)
print(r.text)
print(r.text)
