# Analiza i ocena bezpieczeństwa

## Running Keycloak

```bash
docker-compose up
```

*TODO*: add production configuration file with PostgreSQL
*TODO2*: dockerize backend and add it into docker-compose

## Documentation dump

- Keycloak's "securing applications and services" documentation https://www.keycloak.org/docs/latest/securing_apps/index.html
- OpenID implementation in Python: https://github.com/OpenIDC/pyoidc (does not look well maintained unfortunately)
- Python's `requests` docs page on Oauth: https://docs.python-requests.org/en/latest/user/authentication/#oauth-2-and-openid-connect-authentication
- A Keycloak talk I've found on YouTube (looks promising): https://youtu.be/RhvuS1NO76o


## Work breakdown

1. Frontend:
  - List of hydroponic controllers that user can access
  - Data from hydroponic controllers: 5-6 gauges/plots
  - Requests for controllers:
    - Sensor callibration (input form with 2-3 fields)
    - Placeholder for future things (but not really)

2. Backend:
  - API for posting controller metrics
  - API for getting controller metrics
  - API for posting controller config
  - API for getting controller config

3. ESP:
  - Routine sending metrics to backend
  - Routine downloading config from backend

## Plan prezentacji 1

1. Forma realizacji i problematyka:

Zastosowanie rozwiązań SSO do autoryzacji użytkowników w systemie IOT: system uprawnień kontroli nad zdalnie sterowaną automatyczną szklarnią hydroponiczną

2. Cel realizacji oraz założenia:

Umożliwienie zautoryzowanemu klientowi na działanie w systemie w ramach przydzielonych mu uprawnień, zarządzanych przez system SSO

3. Architektura i szczególy techniczne:

- Rest API django
- SSO keycloak
- RBAC

TODO: finish

