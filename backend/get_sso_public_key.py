from base64 import b64decode

from jose import jwt
import requests
from cryptography.hazmat.primitives import serialization

def get_sso_public_key() -> str:
    r = requests.get("https://baczek.me:9081/realms/master/")
    r.raise_for_status()

    key_der_base64 = r.json()["public_key"]
    key_der = b64decode(key_der_base64.encode())
    public_key = serialization.load_der_public_key(key_der)

    return public_key

