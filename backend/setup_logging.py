import logging

DEFAULT_FORMATTER = (
    "%(asctime)s,%(msecs)-3d %(levelname)s [%(filename)s:%(lineno)d] %(message)s"
)


def setup_logging():
    logging.basicConfig(
        format=DEFAULT_FORMATTER, level=logging.info
    )


