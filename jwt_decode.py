from base64 import b64decode

from jose import jwt
import requests
from cryptography.hazmat.primitives import serialization

r = requests.get("https://baczek.me:9081/realms/master/")
r.raise_for_status()
key_der_base64 = r.json()["public_key"]
key_der = b64decode(key_der_base64.encode())
print(key_der)
public_key = serialization.load_der_public_key(key_der)
token = "Token here!"

payload = jwt.decode(token, public_key, algorithms=["RS256"], options={"verify_aud": False})
print(payload)

