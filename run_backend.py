import json
import logging
import os
from datetime import datetime

from fastapi.middleware.cors import CORSMiddleware
import influxdb_client
from fastapi import Depends, FastAPI, HTTPException, Request
from fastapi.responses import JSONResponse
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from jose import jwt
from jose.exceptions import JWTError
from pydantic import BaseModel

from backend.get_sso_public_key import get_sso_public_key

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
public_key = get_sso_public_key()

INFLUXDB_TOKEN = os.environ["INFLUXDB_TOKEN"]
org = "Hydroponika"
bucket = "hydroponika"
client = InfluxDBClient(url="http://baczek.me:8086", token=INFLUXDB_TOKEN)

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_jwt_payload(request: Request):
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        raise HTTPException(status_code=400, detail="No token provided")

    try:
        print(auth_header)
        print(auth_header)
        print(auth_header)
        auth_type, token = auth_header.split(" ", 2)
    except:
        raise HTTPException(
            status_code=400, detail="Malformed token. Are you sure it's 'jwt <token>' ?"
        )

    try:
        jwt_payload = jwt.decode(
            token,
            public_key,
            algorithms=["RS256"],
            options={
                "verify_aud": False,
                "verify_signature": True,
                "verify_exp": True,
            },
        )
    except JWTError as jwt_error:
        raise HTTPException(status_code=401, detail="Invalid token: " + str(jwt_error))
    # logger.error(json.dumps(jwt_payload, indent=2))
    return jwt_payload


def get_user_or_controller_data(request: Request):
    try:
        return get_user_data(request)
    except:
        return get_controller_data(request)


def get_user_data(request: Request):
    jwt_payload = get_jwt_payload(request)
    try:
        user_name = jwt_payload["preferred_username"]
        user_roles = jwt_payload["resource_access"]["master-realm"]["roles"]
    except KeyError:
        raise HTTPException(status_code=400, detail="Missing username or access roles")
    logger.info("%s auth, roles: %s", user_name, user_roles)

    return user_name, user_roles


def get_controller_data(request: Request):
    jwt_payload = get_jwt_payload(request)
    client_id = jwt_payload.get("azp", None)
    if client_id != "hydroponics-controller":
        raise HTTPException(status_code=400, detail="Invalid client ID")


class HydroponicsMetrics(BaseModel):
    water_level: float
    temperature: float
    ph_level: float
    tds_level: float  # Amount of minerals in the water
    water_level_min: float
    water_level_max: float
    light_level: float


class HydroponicsConfig(BaseModel):
    calibration_sensitivity: float
    ec_correction_factor: float
    ph_correction_factor: float


@app.post("/config/{controller_id}")
def post_controller_config(
    controller_id: int,
    config: HydroponicsConfig,
    user_data=Depends(get_user_data),
):
    write_api = client.write_api(write_options=SYNCHRONOUS)
    datapoint = (
        Point("config")
        .tag("controller_id", controller_id)
        .time(datetime.utcnow(), WritePrecision.S)
    )

    for key, value in config.dict().items():
        datapoint.field(key, value)

    write_api.write(bucket, org, datapoint)
    return "Ok"


@app.get("/config/{controller_id}")
def get_hydroponics_config(
    controller_id: int,
    requester_data=Depends(get_user_or_controller_data),
):
    query = f"""from(bucket: "hydroponika")
|> range(start: -72h)
|> filter(fn: (r) => r["controller_id"] == "{controller_id}")
|> filter(fn: (r) => r["_measurement"] == "config")
|> tail(n: 1)
|> yield(name: "last")
"""
    result = run_and_parse_influxdb_query(query)
    return HydroponicsConfig.parse_obj(result)


@app.post("/metrics/{controller_id}")
def post_hydroponics_metrics(
    controller_id: int,
    metrics: HydroponicsMetrics,
    controller_data=Depends(get_controller_data),
):
    write_api = client.write_api(write_options=SYNCHRONOUS)
    datapoint = (
        Point("metrics")
        .tag("controller_id", controller_id)
        .time(datetime.utcnow(), WritePrecision.S)
    )

    for key, value in metrics.dict().items():
        datapoint.field(key, value)

    write_api.write(bucket, org, datapoint)
    return "Ok"


@app.get("/metrics/{controller_id}")
def get_hydroponics_metrics(
    controller_id: int, user_data=Depends(get_user_data)
) -> HydroponicsMetrics:
    query = f"""from(bucket: "hydroponika")
|> range(start: -72h)
|> filter(fn: (r) => r["controller_id"] == "{controller_id}")
|> filter(fn: (r) => r["_field"] == "ph_level" or r["_field"] == "tds_level" or r["_field"] == "temperature" or r["_field"] == "water_level" or r["_field"] == "water_level_max" or r["_field"] == "water_level_min" or r["_field"] == "light_level" )
|> filter(fn: (r) => r["_measurement"] == "metrics")
|> tail(n: 1)
|> yield(name: "last")
"""
    result = run_and_parse_influxdb_query(query)
    return HydroponicsMetrics.parse_obj(result)


def run_and_parse_influxdb_query(query: str) -> dict:
    query_api = client.query_api()
    result = query_api.query(org=org, query=query)

    results = {}
    for table in result:
        for record in table.records:
            results[record.get_field()] = record.get_value()

    return results
