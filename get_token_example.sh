#!/bin/bash

# Taken from https://www.keycloak.org/docs/latest/securing_apps/#_resource_owner_password_credentials_flow

CLIENT_NAME="client name here"
CLIENT_SECRET="secret here"
USERNAME="username here"
PASSWORD="password here"


curl \
  -d "client_id=$CLIENT_NAME" \
  -d "client_secret=$CLIENT_SECRET" \
  -d "username=$USERNAME" \
  -d "password=$PASSWORD" \
  -d "grant_type=password" \
  "http://localhost:8080/realms/master/protocol/openid-connect/token"

