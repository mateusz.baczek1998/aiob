import Keycloak from "keycloak-js";

const keycloak = new Keycloak(
  {
    url: 'https://baczek.me:9081',
    realm: 'master',
    clientId: 'hydroponics'
  }
);

export const initKeycloak = (onAuthenticatedCallback) => {
    keycloak.init({
      config: {
        url: 'https://baczek.me:9081/auth',
        realm: 'master',
        clientId: 'hydroponics'
      },
      initOptions: {
        onLoad: 'login-required',
        checkLoginIframe: true
      }

      }
      ).then(function (authenticated) {
        if (!authenticated) {
          keycloak.login();
        } else {
          onAuthenticatedCallback();
        }
      }).catch((error) => {
        console.log("Authentication failed");
        console.log(error);
      });
}

export const getToken = () => {
  return keycloak.token;
}

export const updateToken = (successCallback) => {
  keycloak.updateToken(5)
  .then(successCallback)
  .catch(keycloak.login);
}