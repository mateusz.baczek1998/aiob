import { getMetrics, getConfig, editConfig } from './HttpService';
import { Card, Container, Form, Button, Col, Row } from 'react-bootstrap';
import React, { useState } from 'react';
import { updateToken } from './KeycloakService';

function MainPage() {
  const [id, setId] = useState(0);
  const [config, setConfig] = useState({
    calibration_sensitivity: 0,
    ec_correction_factor: 0,
    ph_correction_factor: 0
  });
  const [metrics, setMetrics] = useState({
    water_level: 0,
    temperature: 0,
    ph_level: 0,
    tds_level: 0,
    water_level_min: 0,
    water_level_max: 0,
    light_level: 0
  });
  const [deviceError, setDeviceError] = useState("");
  const [operationStatusColorClass, setOperationStatusColorClass] = useState("");

  const getButtons = () => {
    var devices = [1, 2, 3, 4, 5, 6];

    return devices.map((index) => {
      return <Col><div className='d-grid'><Button onClick={() => handleClick(index)}>Device {index}</Button></div></Col>;
    });
  }

  const handleDeviceError = (error) => {
    setOperationStatusColorClass("text-danger");
    setDeviceError("Device unsupported");
    setConfig({
      calibration_sensitivity: 0,
      ec_correction_factor: 0,
      ph_correction_factor: 0
    });
    setMetrics({
      water_level: 0,
      temperature: 0,
      ph_level: 0,
      tds_level: 0,
      water_level_min: 0,
      water_level_max: 0,
      light_level: 0
    });
  }

  const handleConfigEdit = () => {
    var postData = () => {
    editConfig(id, config, postConfigSuccess, postConfigError);
    }
    updateToken(postData);
  }

  const getDevicesCard = () => {
    return <Card.Body>
      <Card.Title>Choose device</Card.Title>
      <Container>
        <Row>
          {getButtons()}
        </Row>
      </Container>
    </Card.Body>
  }

  const getConfigCard = () => {
    return <Card>
      <Card.Body>
        <Card.Title>Device config</Card.Title>
        <Form onSubmit={stopSubmit}>
          <Form.Group>
            <Form.Label>Calibration sensitivity</Form.Label>
            <Form.Control value={config.calibration_sensitivity} onChange={(event) => { setConfig({ ...config, calibration_sensitivity: parseFloat(event.target.value) }); }} type="number" />

            <Form.Label>Ec correction factor</Form.Label>
            <Form.Control value={config.ec_correction_factor} onChange={(event) => { setConfig({ ...config, ec_correction_factor: parseFloat(event.target.value) }); }} type="number" />

            <Form.Label>Ph correction factor</Form.Label>
            <Form.Control value={config.ph_correction_factor} onChange={(event) => { setConfig({ ...config, ph_correction_factor: parseFloat(event.target.value) }); }} type="number" />
          </Form.Group>
        </Form>
        <div className='d-grid mt-2' >
          <Button onClick={handleConfigEdit}>
            Edit device config
          </Button>
        </div>
        <div className='d-grid mt-2' >
          <Button href="https://baczek.me:3001/d/oKVIKy_7k/hydroponika?orgId=1">
            Go to grafana
          </Button>
        </div>
      </Card.Body>
    </Card>
  }

  const getMetricsCard = () => {
    return <Card>
      <Card.Body>
        <Card.Title>Device metrics</Card.Title>
        <Form onSubmit={stopSubmit} >
          <Form.Group>
            <Form.Label>Water level</Form.Label>
            <Form.Control value={metrics.water_level} type="number" disabled />

            <Form.Label>Temperature</Form.Label>
            <Form.Control value={metrics.temperature} type="number" disabled />

            <Form.Label>Ph level</Form.Label>
            <Form.Control value={metrics.ph_level} type="number" disabled />

            <Form.Label>Tds level</Form.Label>
            <Form.Control value={metrics.tds_level}  type="number" disabled />

            <Form.Label>Minimal water level</Form.Label>
            <Form.Control value={metrics.water_level_min}  type="number" disabled />

            <Form.Label>Maximal water level</Form.Label>
            <Form.Control value={metrics.water_level_max} type="number" disabled />

            <Form.Label>Light level</Form.Label>
            <Form.Control value={metrics.light_level} type="number" disabled />
          </Form.Group>
        </Form>
      </Card.Body>
    </Card>
  }

  const stopSubmit = (event) => {
    event.preventDefault();
  }

  const postConfigSuccess = (value) => {
    setOperationStatusColorClass("text-success");
    setDeviceError("Device data updated");
  }

  const postConfigError = (value) => {
    setOperationStatusColorClass("text-danger");
    setDeviceError("Update failed");
  }

  const getConfigSuccess = (value) => {
    setOperationStatusColorClass("text-success");
    setDeviceError("Device data collected");
    setConfig(value);
  }

  const getMetricsSuccess = (value) => {
    setOperationStatusColorClass("text-success");
    setDeviceError("Device data collected");
    setMetrics(value);
  }

  const handleClick = (id) => {
    var getData = () => {
      getConfig(id, getConfigSuccess, handleDeviceError);
      getMetrics(id, getMetricsSuccess, handleDeviceError);
    }
    if (id > 0) {
      setId(id);
      updateToken(getData);
    }
  }

  return (
    <Container>
      <Row>
        <p class={operationStatusColorClass + " fs-5 mt-2"}>{deviceError}</p>
      </Row>
      <Row className='mt-2'>
        <Col>
          <Card>
            {getDevicesCard()}
          </Card>
        </Col>
      </Row>
      <Row className='mt-2'>
        <Col>
          {getConfigCard()}
        </Col>
        <Col>
          {getMetricsCard()}
        </Col>
      </Row>
    </Container>
  );
}

export default MainPage;
