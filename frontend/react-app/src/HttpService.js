import axios from 'axios';
import { getToken, updateToken } from './KeycloakService';

axios.interceptors.request.use(
    config => {
      config.headers = {
        'Authorization': 'jwt ' + getToken(),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
      return config;
    },
    error => {
      return Promise.reject(error); 
    }
  )

  export const getConfig = (id, stateCallback, errorCallback) => {
    axios.get("https://baczek.me:9001/config/" + id)
    .then((response) => {
        stateCallback(response.data);
      }, (error) => {
        errorCallback(error);
      });
  }

  export const getMetrics = (id, stateCallback, errorCallback) => {
    axios.get("https://baczek.me:9001/metrics/" + id)
    .then((response) => {
        stateCallback(response.data);
      }, (error) => {
        errorCallback(error);
      });
  }

  export const editConfig = (id, load, successCallback, errorCallback) => {
    axios.post("https://baczek.me:9001/config/" + id, load)
    .then((response) => {
      successCallback(response);
      }, (error) => {
        errorCallback(error);
      });
  }
