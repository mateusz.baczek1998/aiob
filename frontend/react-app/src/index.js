import React from 'react';
import './index.css';
import App from './App';
import { createRoot } from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import { initKeycloak } from './KeycloakService';

const container = document.getElementById('root');
const root = createRoot(container);

const renderApp = () => root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

initKeycloak(renderApp);